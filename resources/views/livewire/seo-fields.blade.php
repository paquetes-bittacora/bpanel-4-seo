<div class="widget-box widget-color-blue ui-sortable-handle" id="widget-box-7">
    <div class="widget-header widget-header-small">
        <h6 class="widget-title smaller">{{ __('utils::seo.configuration') }}</h6>
    </div>

    <div class="widget-body">
        <div class="widget-main">
            @livewire('form::input-text', ['name' => 'slug', 'labelText' => __('utils::seo.slug'), 'required'=>true, 'value' => $model->slug ?? ''])
            @livewire('form::input-text', ['name' => 'meta_title', 'labelText' => __('utils::seo.meta_title'), 'required'=>false, 'value' => $model->meta_title ?? ''])
            @livewire('form::input-text', ['name' => 'meta_description', 'labelText' => __('utils::seo.meta_description'), 'required'=>false, 'value' => $model->meta_description ?? ''])
            @livewire('form::input-text', ['name' => 'meta_keywords', 'labelText' => __('utils::seo.meta_keywords'), 'required'=>false, 'value' => $model->meta_keywords ?? ''])
        </div>
    </div>
</div>

<?php

namespace Bittacora\Seo\Http\Livewire;

use Livewire\Component;

class SeoFields extends Component
{
    public  $model;
    
    public function render()
    {
        return view('seo::livewire.seo-fields');
    }
}

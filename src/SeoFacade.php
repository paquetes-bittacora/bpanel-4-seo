<?php

namespace Bittacora\Seo;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Seo\Seo
 */
class SeoFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'seo';
    }
}

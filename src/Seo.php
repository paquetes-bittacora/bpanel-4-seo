<?php

namespace Bittacora\Seo;

use CodeZero\UniqueTranslation\UniqueTranslationRule;
use Illuminate\Database\Schema\Blueprint;

class Seo
{
    /**
     * Define las columnas correspondientes al SEO en una migración.
     * @param Blueprint $table El $table con el que se está definiendo la migración.
     */
    public function addDatabaseFields(Blueprint $table)
    {
        $table->json('slug');
        $table->json('meta_title');
        $table->json('meta_description');
        $table->json('meta_keywords');
    }

    /**
     * Añade las reglas de validación de los campos del panel de SEO a un Request.
     *
     * A la hora de crear un nuevo contenido sólo se comprobará que el campo slug sea único (y que el resto no sean
     * excesivamente largos) pero a la hora de actualizar un contenido, habrá que pasar como mínimo el parámetro
     * $modelId, que será el ID del modelo que se esté guardando. Por defecto se asume que el campo de la base de datos
     * en el que está el ID del modelo se llama 'id', pero si no es así se puede pasar el parámetro $idFieldName
     * indicando en qué campo debe buscarse el ID.
     *
     * @param array $rules Las reglas de validación que se hayan definido en el método rules() del request que se está
     *                     creando (es decir, las reglas que se declaran normalmente, pero pasadas a una variable).
     * @param string $tableName Nombre de la tabla donde se guardarán los campos SEO (normalmente la misma que la del
     *                          modelo).
     * @param int|null $modelId Si se está actualizando un contenido, el ID de ese contenido.
     * @param string|null $idFieldName Si el campo de la base de datos donde está el ID no se llama 'id', indicar en
     *                                 este parámetro el nombre del campo.
     * @return array|string[]
     */
    public function addRequestValidationRules(
        array $rules,
        string $tableName,
        ?int $modelId = null,
        ?string $idFieldName = 'id'
    ) {
        $commonRules = [
            'meta_title' => 'max:255',
            'meta_description' => 'max:160', // Ver https://moz.com/learn/seo/meta-description#:~:text=Optimal%20length,descriptions%20between%2050%E2%80%93160%20characters.
            'meta_keywords' => 'max:255'
        ];

        if (!isset($modelId)) {
            $slugRules = [
                'slug' => [
                    'alpha_dash',
                    'max:255',
                    'required',
                    UniqueTranslationRule::for($tableName)
                ],
            ];
        } else {
            $slugRules = [
                'slug' => [
                    'alpha_dash',
                    'max:255',
                    'required',
                    UniqueTranslationRule::for($tableName)->ignore($modelId, $idFieldName)
                ]
            ];
        }

        return array_merge($rules, $commonRules, $slugRules);
    }
}

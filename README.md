**Ver la documentación de los métodos de la Facade para ver una explicación más
detallada de los parámetros que admiten.**

# Añadir campos seo a una migración:

```php
SeoFacade::addDatabaseFields($table);
```

#Añadir reglas de validación a un Request

En el método `rules()` del `Request` correspondiente, hay que pasar las reglas
que se usan normalmente a un array, para luego pasárselo a la fachada del módulo
y que añada las reglas correspondientes al seo.

```php
public function rules()
{
    $rules = [
        'title' => 'required|max:255',
        // Reglas del resto del formulario...
    ];
    
    $rules = SeoFacade::addRequestValidationRules($rules, 'page');
    
    return $rules;
}
```

# Campos del modelo

Hay que añadir los siguientes campos a las propiedades `$fillable` y `$translatable`
del modelo que vaya a tener los campos SEO

```
'slug',
'meta_title',
'meta_description',
'meta_keywords',
```
